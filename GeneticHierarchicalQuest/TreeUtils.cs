﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GeneticHierarchicalQuest

{
    public class BranchList
    {
        public List<List<ActionStatement>> branches;

        public BranchList()
        {
            branches = new List<List<ActionStatement>>();
        }

        public BranchList(List<ActionStatement>? branch)
        {
            branches = new List<List<ActionStatement>>();
            if (branch != null)
            {
                branches.Add(branch);
            }
        }

        public BranchList DeepClone()
        {
            BranchList branchListNew = new BranchList();

            foreach (var state in this.branches)
            {
                List<ActionStatement> stateNew = new List<ActionStatement>();
                foreach (ActionStatement statement in state)
                {
                    stateNew.Add(statement.DeepClone());
                }
                branchListNew.branches.Add(stateNew);
            }

            return branchListNew;

        }
    }
    public class PlanTreeNode
    {
        public int depthLevel;
        public double branchEval = 0;
        public List<ActionStatement> initState;
        public List<ActionStatement> plan = new();
        public List<PlanTreeNode> goalStates = new();

        public PlanTreeNode()
        {
            //this Constructor initializes the initState
            this.initState = new List<ActionStatement>();
        }
        public PlanTreeNode(List<ActionStatement> state)
        {
            //this Constructor assigns a prebuilt state to initState
            this.initState = state;
        }

        public List<BranchList> inorder(BranchList? branchList = null, List<BranchList>? allRootToLeaf = null)
        {
            //this Function will return List of all root to leaf paths 
            if (branchList == null)
            {
                branchList = new BranchList(this.initState);
            }
            if (allRootToLeaf == null)
            {
                allRootToLeaf = new List<BranchList>();
            }

            if (this.goalStates.Count == 0)
            {
                //foreach (int i in branchList)
                //{
                //    Console.Write(i);
                //}
                //Console.WriteLine();


                allRootToLeaf.Add(branchList.DeepClone());
                branchList.branches.RemoveAt(branchList.branches.Count - 1);

                return allRootToLeaf;
            }

            foreach (PlanTreeNode node in this.goalStates)
            {
                branchList.branches.Add(node.initState);
                allRootToLeaf = node.inorder(branchList, allRootToLeaf);
            }
            branchList.branches.RemoveAt(branchList.branches.Count - 1);
            return allRootToLeaf;
        }

    }
}
