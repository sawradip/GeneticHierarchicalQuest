﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization.Formatters.Binary;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace GeneticHierarchicalQuest
{

    public class ActionStatement
    {
        string actionStringRaw = "";
        public bool ifWithNot = false;
        public List<string> actionList = new List<string>();

        public ActionStatement() { }
        public ActionStatement(string actionString)
        {
            this.actionStringRaw = actionString;
            actionString = actionString.Substring(1, actionString.Length - 2).Trim();

            foreach (string s in actionString.Split(" ").ToList())
            {
                this.actionList.Add(s);
            }
        }
        public ActionStatement(string actionString, bool ifWithNot)
        {
            this.ifWithNot = ifWithNot;
            this.actionStringRaw = actionString;
            actionString = actionString.Trim();
            if (this.ifWithNot)
            {
                int startIndex = actionString.IndexOf("(", 3);
                int endIndex = actionString.IndexOf(")");
                actionString = actionString.Substring(startIndex + 1, endIndex - startIndex - 2);

            }
            else
            {
                actionString = actionString.Substring(1, actionString.Length - 2).Trim();
            }

            foreach (string s in actionString.Split(" ").ToList())
            {
                this.actionList.Add(s);
            }
        }
        public ActionStatement(string action, params string[] paramList)
        {
            this.actionList.Add(action);
            foreach (string param in paramList)
            {
                this.actionList.Add(param);
            }
        }
        public string toString()
        {
            if (this.actionStringRaw != "")
            {
                return this.actionStringRaw;
            }

            string actionstring = "( ";

            foreach (string p in this.actionList)
            {
                actionstring += (p + " ");
            }

            actionstring += ")";

            if (this.ifWithNot)
            {
                actionstring = $"(not{actionstring})";
            }
            return actionstring;
        }

        public ActionStatement DeepClone()
        {
            return new ActionStatement(this.toString(), this.ifWithNot);
        }
    }

    public class Predicate
    {
        public bool initialstate;
        public bool goalstate;
        public int numParams;
        public string opposite = "";
        public List<PredicateParam> paramList = new();

        public Predicate(bool _initialstate, bool _goalstate, int _numparams)
        {
            this.initialstate = _initialstate;
            this.goalstate = _goalstate;
            this.numParams = _numparams;
        }

    }

    public class Action
    {
        public Dictionary<string, string> parameters = new();
        public List<ActionStatement> precondition = new();
        public List<ActionStatement> effect = new();
        public char tensionLevel;

        public Action(char c)
        {
            this.tensionLevel = c;
        }
    }

    public class PredicateParam
    {
        public bool isUnique = false;
        List<string> paramAlternates = new List<string>();

        public PredicateParam() { }
        public PredicateParam(params string[] paramList)
        {
            foreach (string param in paramList)
            {
                paramAlternates.Add(param);
            }
        }
        public PredicateParam(bool _isUnique, params string[] paramList)
        {
            this.isUnique = _isUnique;
            foreach (string param in paramList)
            {
                paramAlternates.Add(param);
            }
        }
        public string getParam()
        {
            string myParam = paramAlternates[(new Random()).Next(0, paramAlternates.Count)];
            return myParam;
        }
    }

    public class Individual
    {
        public int id;
        public double evalScore = 0;
        public PlanTreeNode root = new PlanTreeNode();
    }
}
