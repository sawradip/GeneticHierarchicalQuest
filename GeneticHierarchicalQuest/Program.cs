﻿
using System;
using System.Reflection;
using System.Runtime.Serialization.Formatters.Binary;

namespace GeneticHierarchicalQuest
{
    class QuestGenerator
    {
        static void Main(string[] args)
        {

            Genetic genetic = new Genetic();
            genetic.RunGeneticAlgorithm();

            //PlanTreeNode root = new PlanTreeNode(0);

            //root.dfsFillup(root, currentLevel: 1, nextFillupValue: 1);

            //root.goalStates.Add(new PlanTreeNode(2));
            //root.goalStates.Add(new PlanTreeNode(3));

            //root.goalStates[0].goalStates.Add(new PlanTreeNode(4));
            //root.goalStates[0].goalStates.Add(new PlanTreeNode(5));
            //root.goalStates[0].goalStates.Add(new PlanTreeNode(6));

            //root.goalStates[0].goalStates[2].goalStates.Add(new PlanTreeNode(8));
            //root.goalStates[0].goalStates[2].goalStates.Add(new PlanTreeNode(9));


            //root.goalStates[1].goalStates.Add(new PlanTreeNode(7));

            //var allR2L = root.inorder();
            //foreach (var r2l in allR2L)
            //{
            //    Console.WriteLine(r2l.Count.ToString());
            //foreach (var n in r2l)
            //{
            //    Console.Write(n.ToString());
            //}
            //Console.WriteLine();
            //}

        }

        //public void PlanTreeHandler()
        //{
        //    PlanTreeNode root = new PlanTreeNode(1);

        //    root.goalStates.Add(new PlanTreeNode(2));
        //    root.goalStates.Add(new PlanTreeNode(3));

        //    root.goalStates[0].goalStates.Add(new PlanTreeNode(4));
        //    root.goalStates[0].goalStates.Add(new PlanTreeNode(5));
        //    root.goalStates[0].goalStates.Add(new PlanTreeNode(6));


        //    root.goalStates[0].goalStates[2].goalStates.Add(new PlanTreeNode(8));
        //    root.goalStates[0].goalStates[2].goalStates.Add(new PlanTreeNode(9));


        //    root.goalStates[1].goalStates.Add(new PlanTreeNode(7));


        //    var allR2L = root.inorder();

        //}
    }



    //                1
    //               / \
    //              2   3
    //            / | \   \
    //           4  5  6   7
    //                / \
    //               8   9

    //public class PlanTreeNode
    //{
    //    public int initState;
    //    public List<List<int>> allRootToLeaf = new List<List<int>>();

    //    public List<PlanTreeNode> goalStates = new();

    //    private int maxTreeChildren = 3;
    //    private int maxTreeDepth = 5;

    //    Config config= new Config();
    //    public PlanTreeNode(int n)
    //    {
    //        initState = n;
    //    }

    //    private List<int> BranchListClone(List<int> branchList)
    //    {
    //        List<int> branchListNew = new List<int>();

    //        foreach (int i in branchList)
    //        {
    //            branchListNew.Add(i);
    //        }

    //        return branchListNew;
    //    }

    //    public List<List<int>> inorder(List<int>? branchList = null, List<List<int>>? allRootToLeaf = null)
    //    {

    //        if (branchList == null)
    //        {
    //            branchList = new List<int> { this.initState };
    //        }
    //        if (allRootToLeaf == null)
    //        {
    //            allRootToLeaf = new List<List<int>>();
    //        }

    //        if (this.goalStates.Count == 0)
    //        {
    //            foreach (int i in branchList)
    //            {
    //                Console.Write(i.ToString() + "-");
    //            }
    //            Console.WriteLine();


    //            allRootToLeaf.Add(BranchListClone(branchList));

    //            branchList.RemoveAt(branchList.Count - 1);
    //            return allRootToLeaf;
    //        }

    //        foreach (PlanTreeNode node in this.goalStates)
    //        {
    //            branchList.Add(node.initState);
    //            node.inorder(branchList, allRootToLeaf);
    //        }

    //        branchList.RemoveAt(branchList.Count - 1);
    //        return allRootToLeaf;
    //    }

    //    public int dfsFillup(PlanTreeNode node, int currentLevel, int nextFillupValue) 
    //    { 
    //        if(currentLevel == maxTreeDepth)
    //        {
    //            return nextFillupValue;
    //        }
    //        for(int i = 0; i < maxTreeChildren; i++)
    //        {
    //            PlanTreeNode ptNode = new PlanTreeNode(nextFillupValue);
    //            node.goalStates.Add(ptNode);
    //            nextFillupValue = dfsFillup(ptNode, currentLevel + 1, nextFillupValue + 1);
    //        }

    //        return nextFillupValue;
    //    }

    //}
}
