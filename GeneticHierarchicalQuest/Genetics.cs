﻿using System;
using System.Diagnostics;
using System.Numerics;

namespace GeneticHierarchicalQuest
{
    class Genetic
    {
        Config config = new Config();
        Planner planner = new Planner();
        //WorldDB domainDB = new WorldDB();
        GeneticUtils geneticUtils = new GeneticUtils();


        public void RunGeneticAlgorithm()
        {
            if (Directory.Exists(config.dataDir))
            {
                Directory.Delete(config.dataDir, true);
            }
            Directory.CreateDirectory(config.dataDir);
            Directory.CreateDirectory(config.problemDir);
            Directory.CreateDirectory(config.domainDir);
            Directory.CreateDirectory(config.planDir);

            planner.WriteDomainPDDL();

            List<Individual> currentPopulation = GenerateInitialpopulation();

            //Individual individual = currentPopulation[currentPopulation.Count - 1];
            //Console.WriteLine(individual.root.goalStates[0].goalStates[0].initState.Count);
            //foreach (var ast in individual.root.goalStates[0].goalStates[0].initState)
            //{
            //    Console.WriteLine(ast.toString());
            //}


            for (int genIndex = 0; genIndex < config.numGenerations; genIndex++)
            {
                Console.WriteLine("\nStart new population");
                Stopwatch stopWatch = new Stopwatch();
                stopWatch.Start();

                currentPopulation = EvaluatePopulation(currentPopulation, genIndex);
                //currentPopulation.Sort();
            }
        }


        List<Individual> GenerateInitialpopulation()
        {
            List<Individual> currentPopulation = new List<Individual>();

            for (int popIndex = 0; popIndex < config.populationSize; popIndex++)
            {
                Individual individual = geneticUtils.GenerateIndividual(popIndex);
                currentPopulation.Add(individual);

                //Console.WriteLine(individual.root.goalStates[0].goalStates[0].initState.Count);
                //foreach (var ast in individual.root.goalStates[0].goalStates[0].initState)
                //{
                //    Console.WriteLine(ast.toString());
                //}
                //break;
            }
            return currentPopulation;
        }

        List<Individual> EvaluatePopulation(List<Individual> population, int genIndex)
        {
            string problemGenerationDir = config.problemDir + @"\generation" + genIndex.ToString();
            Directory.CreateDirectory(problemGenerationDir);

            string planGenerationDir = config.planDir + @"\generation" + genIndex.ToString();
            Directory.CreateDirectory(planGenerationDir);


            List<Individual> populationNew = new List<Individual>();
            for (int individualIndex = 0; individualIndex < population.Count; individualIndex++)
            {
                Individual individualNew = EvaluateIndividual(population[individualIndex], individualIndex, genIndex, problemGenerationDir, planGenerationDir);
                populationNew.Add(individualNew);
                //string hash = getIndividualHash(population[individualIndex]);
            }

            return populationNew;
        }

        Individual EvaluateIndividual(Individual individual, int individualIndex, int genIndex, string problemGenerationDir, string planGenerationDir)
        {
            string problemIndividualDir = problemGenerationDir + @"\individual" + individualIndex.ToString();
            Directory.CreateDirectory(problemIndividualDir);

            string planIndividualDir = planGenerationDir + @"\individual" + individualIndex.ToString();
            Directory.CreateDirectory(planIndividualDir);

            Console.WriteLine($"--------------------------------------");
            geneticUtils.EvaluateParentChildPairDFS(individual.root, problemIndividualDir, planIndividualDir);

            Console.WriteLine(geneticUtils.GetEvalScoreDFS(individual.root).Count);
            //string pddlPath = generationDir + @"\individual" + individualIndex.ToString() + ".pddl";
            //string problemName = $"Gen{genIndex}IndividualID{individual.id}p1ID{individual.p1ID}p2ID{individual.p2ID}iSplitID{individual.iSplitID}gSplitID{individual.gSplitID}";
            //planner.WriteProblemPDDL(problemName, individual, pddlPath);

            //List<ActionStatement> pplan = planner.SolveAndPlanEvents(dataDir, individualIndex, genIndex);

            //individual.plan = pplan;
            //if (pplan.Count == 0)
            //{
            //    individual.evaluation = 1;
            //}
            //else
            //{
            //    individual.evaluation = Fitness(pplan);
            //}

            return individual;
        }

    }

}
