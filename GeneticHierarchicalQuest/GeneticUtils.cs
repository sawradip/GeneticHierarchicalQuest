﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace GeneticHierarchicalQuest
{
    class GeneticUtils
    {

        Random random = new Random();
        Config config = new Config();
        WorldDB domainDB = new WorldDB();
        Planner planner = new Planner();
        Metrics metrics = new Metrics();
        Validation validation = new Validation();

        public Individual GenerateIndividual(int popIndex)
        {
            Individual individual = new Individual();

            int numInitFacts = random.Next(config.maxFactsInitState);

            foreach (ActionStatement statement in domainDB.WorldStaticInits)
            {
                individual.root.initState.Add(statement);
            }

            for (int i = 0; i < numInitFacts; i++)
            {
                ActionStatement randomfact = GenerateRandomFact(individual.root.initState, isinit: true);
                if (randomfact.actionList.Count == 0)
                {
                    continue;
                }
                int numExpectedParam = domainDB.WorldPredicates[randomfact.actionList[0]].paramList.Count;
                if (randomfact.actionList.Count == (numExpectedParam + 1))
                {
                    individual.root.initState.Add(randomfact);
                    //Console.WriteLine($"Population{popIndex} - {randomfact.toString()}");
                }

            }
            //this adds Goals to the memory, inplace.
            GetGoalStatesDFS(individual.root, currentDepth: 1);

            return individual;
        }

        public ActionStatement GenerateRandomFact(List<ActionStatement> state, bool isinit = false, bool isgoal = false)
        {
            for (int i = 0; i < config.maxValidPredicateTries; i++)
            {
                string randPredicate = domainDB.WorldPredicates.ElementAt(random.Next(0, domainDB.WorldPredicates.Count)).Key;
                Predicate predicateInfo = domainDB.WorldPredicates[randPredicate];
                bool validLocation = true;

                if (isinit && !(predicateInfo.initialstate))
                {
                    validLocation = false;
                }
                if (isgoal && !(predicateInfo.goalstate))
                {
                    validLocation = false;
                }

                if (validLocation)
                {
                    ActionStatement genActionStep = new ActionStatement();

                    genActionStep.actionList.Add(randPredicate);
                    for (int paramIndex = 0; paramIndex < predicateInfo.paramList.Count; paramIndex++)
                    {
                        PredicateParam paramObj = predicateInfo.paramList[paramIndex];
                        string factParam = GetRandomValidFactParam(paramObj, randPredicate, state, paramIndex);
                        if (factParam == "")
                        {
                            return genActionStep;
                        }
                        genActionStep.actionList.Add(factParam);
                    }

                    if (!validation.isRepeatedPredicateAndParameters(state, genActionStep) && !validation.isOppositePredicateAndParameters(state, genActionStep))
                    {
                        return genActionStep;
                    }
                }
            }
            return new ActionStatement();
        }

        public string GetRandomValidFactParam(PredicateParam paramObj, string predicateName, List<ActionStatement> state, int paramIndex)
        {
            for (int i = 0; i < config.maxValidFactTries; i++)
            {
                string objName = paramObj.getParam();
                string paramName = domainDB.WorldObjects[objName][random.Next(domainDB.WorldObjects[objName].Count)];
                if (paramObj.isUnique)
                {
                    if (validation.isUniquePredicateAndParameterInstance(paramName, predicateName, paramIndex, state))
                    {
                        return paramName;
                    }
                    else
                    {
                        continue;
                    }
                }
                else
                {
                    return paramName;
                }
            }
            return "";
        }

        public void GetGoalStatesDFS(PlanTreeNode node, int currentDepth)
        {
            if (currentDepth == config.maxTreeDepth)
            {
                return;
            }

            node.depthLevel = currentDepth;
            for (int i = 0; i < config.maxTreeChildren; i++)
            {
                int numgoalfacts = random.Next(config.maxFactsGoalState);

                PlanTreeNode ptNode = new PlanTreeNode();

                for (int j = 0; j < numgoalfacts; j++)
                {
                    ActionStatement randomfact = GenerateRandomFact(ptNode.initState, isgoal: true);
                    if (randomfact.actionList.Count == 0)
                    {
                        continue;
                    }
                    int numExpectedParam = domainDB.WorldPredicates[randomfact.actionList[0]].paramList.Count;
                    if (randomfact.actionList.Count == (numExpectedParam + 1))
                    {
                        ptNode.initState.Add(randomfact);
                    }
                }
                node.goalStates.Add(ptNode);
                GetGoalStatesDFS(ptNode, currentDepth + 1);
            }
        }

        public void AddInit2Goal(PlanTreeNode initNode, PlanTreeNode goalNode)
        {
            foreach (ActionStatement initStatement in initNode.initState)
            {
                if (!validation.isRepeatedPredicateAndParameters(goalNode.initState, initStatement) && !validation.isOppositePredicateAndParameters(goalNode.initState, initStatement))
                {
                    goalNode.initState.Add(initStatement);
                }
            }

        }

        public void EvaluateParentChildPairDFS(PlanTreeNode initNode, string individualProblemDir, string individualPlanDir, string problemName = "")
        {
            string problemNameCurrent;
            if (initNode.goalStates.Count == 0)
            {
                return;
            }
            //Console.WriteLine($"Entering depth level {initNode.depthLevel}- Last problemName - {problemName}");
            for (int i = 0; i < config.maxTreeChildren; i++)
            {

                //if (problemName == "")
                //{
                //    problemNameCurrent = "Root";
                //}
                //else
                //{
                //    problemNameCurrent = problemName + $"Child{i}";
                //}
                problemNameCurrent = problemName + $"Child{i}";
                string problemFilePath = $"{individualProblemDir}\\{problemName}.pddl";
                string planFilePath = $"{individualPlanDir}\\{problemName}.ipc";
                var goalNode = initNode.goalStates[i];

                planner.WriteProblemPDDL(problemName, initNode, goalNode, problemFilePath);

                Console.WriteLine(problemNameCurrent);
                List<ActionStatement> pplan = planner.SolveAndPlanEvents(problemFilePath, planFilePath);
                goalNode.plan = pplan;
                if (pplan.Count == 0)
                {
                    goalNode.branchEval = 1;
                }
                else
                {
                    goalNode.branchEval = metrics.Fitness(pplan);
                }
                Console.WriteLine(goalNode.branchEval);
                AddInit2Goal(initNode: initNode, goalNode: goalNode);
                //Console.WriteLine($"{i} of {initNode.goalStates.Count} Children.");
                
                EvaluateParentChildPairDFS(goalNode, individualProblemDir, individualPlanDir, problemName: problemNameCurrent);
                
            }
        }
    

        public List<double> GetEvalScoreDFS(PlanTreeNode initNode, double evalScore = 0, List<double>? allRootToLeafEval = null)
        {
            if (allRootToLeafEval == null)
            {
                allRootToLeafEval = new List<double>();
            }

            if (initNode.goalStates.Count == 0 )
            {
                allRootToLeafEval.Add(evalScore);
                return allRootToLeafEval;
            }

            foreach (PlanTreeNode node in initNode.goalStates)
            {
                double currentEvalScore;
                if(node.branchEval <= 1)
                {
                    currentEvalScore = evalScore;
                }
                else
                {
                    currentEvalScore = evalScore + node.branchEval;
                }
                allRootToLeafEval = GetEvalScoreDFS(node, currentEvalScore, allRootToLeafEval);
            }
            return allRootToLeafEval;
        }

        
    }
}
