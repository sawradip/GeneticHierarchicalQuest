﻿using System;

namespace GeneticHierarchicalQuest
{
    class Config
    {
        public string rootDir = @"C:\Users\Admin\Desktop\practice_code\practice_csharp\GeneticHierarchicalQuest\GeneticHierarchicalQuest";
        public string dataDir;
        public string domainDir;
        public string problemDir;
        public string planDir;
        public string staticFilesDir;

        public string domainFilePath = "";
        public string jarFilepath = "";

        public int populationSize = 50;
        public int numGenerations = 200;

        public int maxFactsInitState = 5;
        public int maxFactsGoalState = 20;

        public int maxValidPredicateTries = 5;
        public int maxValidFactTries = 5;

        public int normalizeScaleLength = 10;
        public string desiredStoryArc = "==+++==---";

        public int elitismFactor = 2;
        public int mutationProbability = 20;

        public int maxTreeDepth = 4;
        public int maxTreeChildren = 3;

        public int solverPatienceMilliSeconds = 5000;
        public Config()
        {
            this.dataDir = rootDir + @"\data";

            this.domainDir = dataDir + @"\domain";
            this.problemDir = dataDir + @"\problems";
            this.planDir = dataDir + @"\plans";

            this.staticFilesDir = rootDir + @"\staticFiles";
            this.jarFilepath = staticFilesDir + @"\pddl4j-3.8.3.jar";
            this.domainFilePath = domainDir + @"\OurDomain.pddl";

        }
    }
}
