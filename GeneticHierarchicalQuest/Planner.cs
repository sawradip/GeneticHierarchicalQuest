﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace GeneticHierarchicalQuest
{
    public class Planner
    {

		//change 
         //public string problemName = "";
        WorldDB domainDB = new WorldDB();
        Config config = new Config();


        public void WriteDomainPDDL()
        {
            string pddlString = "";
            char[] alpha = "ABCDEFGHIJKLMNOPQRSTUVWXYZ".ToCharArray();
            pddlString += $"(define (domain {domainDB.domainName})\n\n"; ;
            pddlString += "\t(:requirements :strips  :typing  :equality :negative-preconditions)\n\n";
            pddlString += "\t(:types";

            foreach (var worldObj in domainDB.WorldObjects)
            {
                pddlString += $" {worldObj.Key}";
            }

            pddlString += " )\n\n";
            pddlString += "\t(:predicates\n";

            foreach (var item in domainDB.WorldPredicates)
            {
                ActionStatement predStatement = new ActionStatement();

                predStatement.actionList.Add(item.Key);

                for (int i = 0; i < item.Value.paramList.Count; i++)
                {
                    string tempVar = "?" + alpha[i];
                    predStatement.actionList.Add(tempVar);
                }
                pddlString += "\t\t" + predStatement.toString() + "\n";
            }
            pddlString += "\t)\n";

            foreach (var action in domainDB.WorldActions)
            {
                pddlString += $"\t(:action {action.Key}\n";
                pddlString += "\t\t:parameters (\n";

                foreach (var paramItem in action.Value.parameters)
                {
                    pddlString += $"\t\t\t{paramItem.Key} - {paramItem.Value}\n";
                }
                pddlString += "\t\t)\n";
                pddlString += "\t\t:precondition (and\n";

                foreach (ActionStatement s in action.Value.precondition)
                {
                    pddlString += $"\t\t\t{s.toString()}\n";
                }
                pddlString += "\t\t)\n";
                pddlString += "\t\t:effect (and\n";
                foreach (ActionStatement s in action.Value.effect)
                {
                    pddlString += $"\t\t\t{s.toString()}\n";
                }
                pddlString += "\t\t)\n";
                pddlString += "\t)\n";
            }

            pddlString += ")\n";

            List<string> lines = pddlString.Split(@"\n").ToList();
            File.WriteAllLines(config.domainFilePath, lines);
            //Console.WriteLine(pddlString);
        }


        public void WriteProblemPDDL(string problemName, PlanTreeNode initNode, PlanTreeNode goalNode, string problemFilePath)
        {
            string pddlString = "";

            pddlString += $"(define (problem {problemName}) (:domain {domainDB.domainName})\n"; ;
            pddlString += "\t(:objects\n";

            foreach (var worldObj in domainDB.WorldObjects)
            {
                foreach (string entity in worldObj.Value)
                {
                    pddlString += $"\t\t{entity} - {worldObj.Key}\n";
                }
            }

            pddlString += "\t)\n";
            pddlString += "\t(:init\n";
            foreach (ActionStatement statement in initNode.initState)
            {
                pddlString += $"\t\t{statement.toString()}\n";
            }

            pddlString += "\t)\n";
            pddlString += "\t(:goal (and\n";

            pddlString += "\t\t(and\n";

            foreach (ActionStatement statement in goalNode.initState)
            {
                pddlString += $"\t\t\t{statement.toString()}\n";
            }

            pddlString += "\t\t)\n";
            pddlString += "\t))\n";
            pddlString += ")\n";
            //Console.WriteLine( pddlString );
            List<string> lines = pddlString.Split(@"\n").ToList();
            File.WriteAllLines(problemFilePath, lines);
            //Console.WriteLine(pddlString);

        }
        public bool RegexStartsWith(string statement, string pattern)
        {
            return Regex.Match(statement, "^(" + pattern + ")").Success;
        }

        public List<ActionStatement> SolveAndPlanEvents(string problemFilelPath, string planFilePath)
        {
            //string statiFilesDir = domainDB.rootDir + @"\staticFiles";

            //string domainFile = dataDir + @"\domain\OurDomain.pddl";
            ////string domainFile = statiFilesDir +   @"\OurDomain(1).pddl";
            //string problemFile = dataDir + $"\\problems\\generation{genIndex}\\individual{individualIndex}.pddl";
            //string jarPath = statiFilesDir + @"\pddl4j-3.8.3.jar";

            List<string> planStatements = new List<string>();
            List<ActionStatement> planActionStatements = new List<ActionStatement>();
            try
            {
                Process planner = new Process();
                planner.StartInfo.FileName = "java";
                planner.StartInfo.CreateNoWindow = true;
                planner.StartInfo.Arguments = $"-jar \"{config.jarFilepath}\" -o \"{config.domainFilePath}\" -f \"{problemFilelPath}\"";

                //Console.WriteLine($"-jar {config.domainFilePath}");
                //Console.WriteLine($"-jar \"{config.jarFilepath}\" -o \"{config.domainFilePath}\" -f \"{problemFilelPath}\"");
                planner.StartInfo.UseShellExecute = false;
                planner.StartInfo.RedirectStandardOutput = true;
                planner.Start();
                bool ifExitedAready = planner.WaitForExit(config.solverPatienceMilliSeconds);

                bool isStart = true;
                while (ifExitedAready && !planner.StandardOutput.EndOfStream)
                {
                    string line = planner.StandardOutput.ReadLine() ?? "returned Null";
                    //Console.WriteLine(line);
                    if (RegexStartsWith(line, @"\d+:\s"))
                    {
                        int sStatement = line.IndexOf("(");
                        int eStatement = line.IndexOf(")");
                        string actionString = line.Substring(sStatement, eStatement - sStatement + 1);
                        ActionStatement newPlanStep = new ActionStatement(actionString);


                        if (isStart)
                        {
                            isStart = false;
                            //Console.WriteLine("\n");
                        }
                        //Console.WriteLine(actionString);
                        planStatements.Add(actionString);
                        planActionStatements.Add(newPlanStep);
                    }
                }
            }
            catch (System.Exception e)
            {
                Console.WriteLine(e);
                Console.WriteLine($"Error in Planning:{problemFilelPath}");
            }
            if (planStatements.Count > 0)
            {
                File.WriteAllLines(planFilePath, planStatements);
            }
            Console.Write("|");

            return planActionStatements;
        }


    }
}
