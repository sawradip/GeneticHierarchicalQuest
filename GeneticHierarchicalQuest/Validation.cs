﻿using System;


namespace GeneticHierarchicalQuest { 
    class Validation
    {

        WorldDB domainDB = new WorldDB();
        public bool isUniquePredicateAndParameterInstance(string statementParamName, string predicateName, int paramIndex, List<ActionStatement> state)
        {
            foreach (ActionStatement st in state)
            {
                if ((st.actionList[0] == predicateName) && (st.actionList[paramIndex + 1] == statementParamName))
                {
                    //this predicate contains another statement with this param is exactly this position
                    //where it is supposed to be unique
                    return false;
                }
            }
            //None of the predicates contains another statement with this param is exactly this position
            return true;
        }

        public bool isRepeatedPredicateAndParameters(List<ActionStatement> state, ActionStatement actionStep)
        {
            string statement = actionStep.toString();
            foreach (ActionStatement stateStep in state)
            {
                if (stateStep.toString() == statement)
                {
                    return true;
                }
            }
            return false;
        }

        public bool isOppositePredicateAndParameters(List<ActionStatement> state, ActionStatement actionStep)
        {
            string actionStepPredicateName = actionStep.actionList[0];

            if (domainDB.WorldPredicates[actionStepPredicateName].opposite != "")
            {
                ActionStatement oppositeActionStep = actionStep;
                oppositeActionStep.actionList[0] = domainDB.WorldPredicates[actionStepPredicateName].opposite;

                bool isOpposite = isRepeatedPredicateAndParameters(state, oppositeActionStep);
                return isOpposite;
            }
            else
            {
                return false;
            }
        }
    }
}
