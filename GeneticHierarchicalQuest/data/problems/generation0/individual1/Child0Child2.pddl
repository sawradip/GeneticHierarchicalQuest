(define (problem Child0Child2) (:domain ZombieDomain)
	(:objects
		outdoor1 - location
		outdoor2 - location
		walkway1 - location
		walkway2 - location
		walkway3 - location
		walkway4 - location
		walkway5 - location
		walkway6 - location
		sideway1 - location
		sideway2 - location
		sideway3 - location
		sideway4 - location
		bedroom1 - location
		bedroom2 - location
		john - character
		jane - character
		maria - character
		zombie1 - zombie
		zombie2 - zombie
		zombie3 - zombie
	)
	(:init
		( at zombie3 bedroom1 )
		( safe walkway1 )
		( at john bedroom2 )
		( at zombie2 walkway1 )
		( infected maria )
		( infected john )
		( alive jane )
		( at maria walkway6 )
		( safe walkway3 )
		( infected jane )
		( alive maria )
		( alive john )
		( at jane sideway2 )
		( safe sideway3 )
		( at zombie1 walkway6 )
		( safe bedroom1 )
		( at maria sideway4 )
		(path outdoor2 walkway1)
		(path walkway1 outdoor2)
		(path walkway1 bedroom1)
		(path bedroom1 walkway1)
		(path walkway1 walkway2)
		(path walkway2 walkway1)
		(path walkway2 walkway3)
		(path walkway3 walkway2)
		(path walkway3 bedroom2)
		(path bedroom2 walkway3)
		(path walkway3 walkway4)
		(path walkway4 walkway3)
		(path walkway4 walkway5)
		(path walkway5 walkway4)
		(path walkway4 sideway1)
		(path sideway1 walkway4)
		(path sideway1 sideway2)
		(path sideway2 sideway1)
		(path sideway2 sideway3)
		(path sideway3 sideway2)
		(path sideway3 sideway4)
		(path sideway4 sideway3)
		(path walkway5 walkway6)
		(path walkway6 walkway5)
		(path walkway6 outdoor1)
		(path outdoor1 walkway6)
		(safe walkway6)
		(safe walkway5)
		(safe walkway4)
		(safe walkway3)
		(safe walkway2)
		(safe walkway1)
		(safe outdoor2)
		(safe outdoor1)
		(safe bedroom1)
		(safe bedroom2)
		(safe sideway1)
		(safe sideway2)
		(safe sideway3)
		(safe sideway4)
		( absent john )
		( unborn zombie3 )
	)
	(:goal (and
		(and
			( infected jane )
			( safe outdoor2 )
			( infected john )
			( safe walkway3 )
			( at zombie3 walkway3 )
			( safe bedroom1 )
			( alive maria )
			( at jane walkway2 )
			( infected maria )
			( safe sideway1 )
			( safe walkway2 )
			( at maria sideway3 )
			( alive jane )
			( at zombie1 outdoor2 )
			( safe sideway4 )
		)
	))
)

