(define (domain ZombieDomain)

	(:requirements :strips  :typing  :equality :negative-preconditions)

	(:types location character zombie )

	(:predicates
		( at ?A ?B )
		( path ?A ?B )
		( absent ?A )
		( unborn ?A )
		( alive ?A )
		( safe ?A )
		( infected ?A )
	)
	(:action appearcharacter
		:parameters (
			?c - character
			?l - location
		)
		:precondition (and
			(absent ?c)
			(safe ?l)
		)
		:effect (and
			(not (absent ?c))
			(alive ?c)
			(at ?c ?l)
		)
	)
	(:action appearzombie
		:parameters (
			?z - zombie
			?l - location
		)
		:precondition (and
			(unborn ?z)
		)
		:effect (and
			(not (unborn ?z))
			(not (safe ?l))
			(at ?z ?l)
		)
	)
	(:action gocharacter
		:parameters (
			?c - character
			?l1 - location
			?l2 - location
		)
		:precondition (and
			(alive ?c)
			(path ?l1 ?l2)
			(at ?c ?l1)
		)
		:effect (and
			(at ?c ?l2)
			(path ?l1 ?l2)
			(not (at ?c ?l1))
		)
	)
	(:action gozombie
		:parameters (
			?z - zombie
			?l1 - location
			?l2 - location
		)
		:precondition (and
			(path ?l1 ?l2)
			(at ?z ?l1)
		)
		:effect (and
			(at ?z ?l2)
			(not (at ?z ?l1))
			(safe ?l1)
			(not(safe ?l2))
		)
	)
)

