﻿using System;

namespace GeneticHierarchicalQuest
{
    class WorldDB
    {
        public string domainName;
        public Dictionary<string, char> WorldEvents = new();
        public Dictionary<string, List<string>> WorldObjects = new();
        public Dictionary<string, Predicate> WorldPredicates = new();
        public Dictionary<string, Action> WorldActions = new();
        public List<ActionStatement> WorldStaticInits = new();



        public WorldDB()
        {
            this.domainName = "ZombieDomain";
            LoadWorldObjects();
            LoadWorldPredicates();
            LoadWorldStaticInits();
            LoadWorlActions();
            LoadWorldEvents();
            Console.WriteLine("Loaded all data");
        }


        public void LoadWorldObjects()
        {
            WorldObjects["location"] = new List<string> { "outdoor1", "outdoor2", "walkway1",
                                                        "walkway2", "walkway3", "walkway4",
                                                        "walkway5", "walkway6", "sideway1",
                                                        "sideway2",  "sideway3", "sideway4",
                                                        "bedroom1", "bedroom2" };

            WorldObjects["character"] = new List<string> { "john", "jane", "maria" };
            WorldObjects["zombie"] = new List<string> { "zombie1", "zombie2", "zombie3" };
            //WorldObjects["food"] = new List<string> { "rawfood1" };
            //WorldObjects["antidote"] = new List<string> { "antidote1" };
            //WorldObjects["zombie"] = new List<string> { "zombie1", "zombie2" };
        }
        public void LoadWorldPredicates()
        {
            PredicateParam pp = new PredicateParam();

            WorldPredicates.Add("at", new Predicate(true, true, 2));
            WorldPredicates["at"].paramList.Add(new PredicateParam(true, "character", "zombie"));
            WorldPredicates["at"].paramList.Add(new PredicateParam("location"));

            WorldPredicates.Add("path", new Predicate(false, false, 2));
            WorldPredicates["path"].paramList.Add(new PredicateParam("location"));
            WorldPredicates["path"].paramList.Add(new PredicateParam("location"));

            WorldPredicates.Add("absent", new Predicate(true, false, 1));
            WorldPredicates["absent"].paramList.Add(new PredicateParam("character"));

            WorldPredicates.Add("unborn", new Predicate(true, false, 1));
            WorldPredicates["unborn"].paramList.Add(new PredicateParam("zombie"));

            WorldPredicates.Add("alive", new Predicate(false, true, 1));
            WorldPredicates["alive"].paramList.Add(new PredicateParam("character"));

            WorldPredicates.Add("safe", new Predicate(true, true, 1));
            WorldPredicates["safe"].paramList.Add(new PredicateParam("location"));

            WorldPredicates.Add("infected", new Predicate(true, true, 1));
            //WorldPredicates["infected"].opposite = "cured";
            WorldPredicates["infected"].paramList.Add(new PredicateParam("character"));

        }

        public void LoadWorlActions()
        {
            WorldActions["appearcharacter"] = new Action('+');
            WorldActions["appearcharacter"].parameters =
                new Dictionary<string, string>() {  { "?c", "character" },
                                                    { "?l", "location"} };
            WorldActions["appearcharacter"].precondition =
                new List<ActionStatement> { new ActionStatement("(absent ?c)"),
                                            new ActionStatement("(safe ?l)")};
            WorldActions["appearcharacter"].effect =
                new List<ActionStatement> { new ActionStatement("(not (absent ?c))", ifWithNot : true),
                                            new ActionStatement("(alive ?c)"),
                                            new ActionStatement("(at ?c ?l)"), };

            WorldActions["appearzombie"] = new Action('+');
            WorldActions["appearzombie"].parameters =
                new Dictionary<string, string>() {  { "?z", "zombie" },
                                                    { "?l", "location"} };
            WorldActions["appearzombie"].precondition =
                new List<ActionStatement> { new ActionStatement("(unborn ?z)") };
            WorldActions["appearzombie"].effect =
                new List<ActionStatement> { new ActionStatement("(not (unborn ?z))",  ifWithNot : true),
                                            new ActionStatement("(not (safe ?l))",  ifWithNot : true),
                                            new ActionStatement("(at ?z ?l)"), };

            WorldActions["gocharacter"] = new Action('=');
            WorldActions["gocharacter"].parameters =
                new Dictionary<string, string>() {  { "?c", "character" },
                                                    { "?l1", "location"},
                                                    { "?l2", "location"} };
            WorldActions["gocharacter"].precondition =
                new List<ActionStatement> { new ActionStatement("(alive ?c)"),
                                            new ActionStatement("(path ?l1 ?l2)"),
                                            new ActionStatement("(at ?c ?l1)")};
            WorldActions["gocharacter"].effect =
                new List<ActionStatement> { new ActionStatement("(at ?c ?l2)"),
                                            new ActionStatement("(path ?l1 ?l2)"),
                                            new ActionStatement("(not (at ?c ?l1))",  ifWithNot : true)};


            WorldActions["gozombie"] = new Action('=');
            WorldActions["gozombie"].parameters =
                new Dictionary<string, string>() {  { "?z", "zombie" },
                                                    { "?l1", "location"},
                                                    { "?l2", "location"} };
            WorldActions["gozombie"].precondition =
                new List<ActionStatement> { new ActionStatement("(path ?l1 ?l2)"),
                                            new ActionStatement("(at ?z ?l1)")};
            WorldActions["gozombie"].effect =
                new List<ActionStatement> { new ActionStatement("(at ?z ?l2)"),
                                            new ActionStatement("(not (at ?z ?l1))",  ifWithNot : true),
                                            new ActionStatement("(safe ?l1)"),
                                            new ActionStatement("(not(safe ?l2))",  ifWithNot : true)};



        }



        public void LoadWorldStaticInits()
        {

            // ; path
            WorldStaticInits.Add(new ActionStatement("(path outdoor2 walkway1)"));
            WorldStaticInits.Add(new ActionStatement("(path walkway1 outdoor2)"));
            WorldStaticInits.Add(new ActionStatement("(path walkway1 bedroom1)"));
            WorldStaticInits.Add(new ActionStatement("(path bedroom1 walkway1)"));
            WorldStaticInits.Add(new ActionStatement("(path walkway1 walkway2)"));
            WorldStaticInits.Add(new ActionStatement("(path walkway2 walkway1)"));
            WorldStaticInits.Add(new ActionStatement("(path walkway2 walkway3)"));
            WorldStaticInits.Add(new ActionStatement("(path walkway3 walkway2)"));
            WorldStaticInits.Add(new ActionStatement("(path walkway3 bedroom2)"));
            WorldStaticInits.Add(new ActionStatement("(path bedroom2 walkway3)"));
            WorldStaticInits.Add(new ActionStatement("(path walkway3 walkway4)"));
            WorldStaticInits.Add(new ActionStatement("(path walkway4 walkway3)"));
            WorldStaticInits.Add(new ActionStatement("(path walkway4 walkway5)"));
            WorldStaticInits.Add(new ActionStatement("(path walkway5 walkway4)"));
            WorldStaticInits.Add(new ActionStatement("(path walkway4 sideway1)"));
            WorldStaticInits.Add(new ActionStatement("(path sideway1 walkway4)"));
            WorldStaticInits.Add(new ActionStatement("(path sideway1 sideway2)"));
            WorldStaticInits.Add(new ActionStatement("(path sideway2 sideway1)"));
            WorldStaticInits.Add(new ActionStatement("(path sideway2 sideway3)"));
            WorldStaticInits.Add(new ActionStatement("(path sideway3 sideway2)"));
            WorldStaticInits.Add(new ActionStatement("(path sideway3 sideway4)"));
            WorldStaticInits.Add(new ActionStatement("(path sideway4 sideway3)"));
            WorldStaticInits.Add(new ActionStatement("(path walkway5 walkway6)"));
            WorldStaticInits.Add(new ActionStatement("(path walkway6 walkway5)"));
            WorldStaticInits.Add(new ActionStatement("(path walkway6 outdoor1)"));
            WorldStaticInits.Add(new ActionStatement("(path outdoor1 walkway6)"));
            //; safe
            WorldStaticInits.Add(new ActionStatement("(safe walkway6)"));
            WorldStaticInits.Add(new ActionStatement("(safe walkway5)"));
            WorldStaticInits.Add(new ActionStatement("(safe walkway4)"));
            WorldStaticInits.Add(new ActionStatement("(safe walkway3)"));
            WorldStaticInits.Add(new ActionStatement("(safe walkway2)"));
            WorldStaticInits.Add(new ActionStatement("(safe walkway1)"));
            WorldStaticInits.Add(new ActionStatement("(safe outdoor2)"));
            WorldStaticInits.Add(new ActionStatement("(safe outdoor1)"));
            WorldStaticInits.Add(new ActionStatement("(safe bedroom1)"));
            WorldStaticInits.Add(new ActionStatement("(safe bedroom2)"));
            WorldStaticInits.Add(new ActionStatement("(safe sideway1)"));
            WorldStaticInits.Add(new ActionStatement("(safe sideway2)"));
            WorldStaticInits.Add(new ActionStatement("(safe sideway3)"));
            WorldStaticInits.Add(new ActionStatement("(safe sideway4)"));
            ////;absent
            //WorldStaticInits.Add(new ActionStatement("(absent john)"));
            //WorldStaticInits.Add(new ActionStatement("(absent jane)"));
            //WorldStaticInits.Add(new ActionStatement("(absent maria)"));
            ////;unborn
            //WorldStaticInits.Add(new ActionStatement("(unborn zombie1)"));
            //WorldStaticInits.Add(new ActionStatement("(unborn zombie2)"));
            //WorldStaticInits.Add(new ActionStatement("(unborn zombie3)"));


        }
        public void LoadWorldEvents()
        {
            //WorldEvents["gocharacter"] = '=';
            foreach (var item in WorldActions)
            {
                WorldEvents[item.Key] = item.Value.tensionLevel;
            }
        }
    }

}

