﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GeneticHierarchicalQuest
{
    class Metrics
    {
        WorldDB domainDB = new WorldDB();
        Config config = new Config();
        public double Fitness(List<ActionStatement> pplan)
        {
            List<char> storyArc = getStoryArc(pplan);
            List<int> storyArcNum = new List<int>() { 0 };
            List<int> storyArcNormalized = new List<int>();
            List<int> desiredArcNum = new List<int>() { 0 };
            List<int> desiredArcNormalized = new List<int>();

            foreach (char c in storyArc)
            {
                if (c == '+')
                {
                    storyArcNum.Add(storyArcNum.Last() + 1);
                }
                if (c == '-')
                {
                    storyArcNum.Add(storyArcNum.Last() - 1);
                }
                if (c == '=')
                {
                    storyArcNum.Add(storyArcNum.Last());
                }
            }

            foreach (char c in config.desiredStoryArc)
            {
                if (c == '+')
                {
                    desiredArcNum.Add(desiredArcNum.Last() + 1);
                }
                if (c == '-')
                {
                    desiredArcNum.Add(desiredArcNum.Last() - 1);
                }
                if (c == '=')
                {
                    desiredArcNum.Add(desiredArcNum.Last());
                }
            }


            for (int i = 0; i < config.normalizeScaleLength; i++)
            {
                int storyIndex = (int)Math.Ceiling((double)(storyArcNum.Count - 1) * (i / (double)config.normalizeScaleLength));
                storyArcNormalized.Add(storyArcNum[storyIndex]);
                int desiredIndex = (int)Math.Ceiling((double)(desiredArcNum.Count - 1) * (i / (double)config.normalizeScaleLength));
                desiredArcNormalized.Add(desiredArcNum[desiredIndex]);
            }

            double mse = MeanSquaredError(storyArcNormalized, desiredArcNormalized);

            if (mse == 0)
            {
                mse = 1;
            }

            double evaluation = 1 + (pplan.Count / mse);
            return evaluation;
        }

        List<char> getStoryArc(List<ActionStatement> pplan)
        {
            List<char> arc = new List<char>();

            foreach (ActionStatement s in pplan)
            {
                char c = domainDB.WorldEvents[s.actionList[0]];
                arc.Add(c);
            }
            return arc;
        }

        double MeanSquaredError(List<int> storyArcNormalized, List<int> desiredArcNormalized)
        {
            double mse = 0;
            for (int i = 0; i < config.normalizeScaleLength; i++)
            {
                mse += Math.Pow((storyArcNormalized[i] - desiredArcNormalized[i]), 2);
            }

            return mse / config.normalizeScaleLength;
        }
    }
}
