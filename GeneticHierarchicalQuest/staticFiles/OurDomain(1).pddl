;Header and description

(define (domain GetDone)


    (:requirements :strips  :typing  :equality :negative-preconditions)

    (:types location zombie food antidote character )


    (:predicates
        (at ?c ?l)
        (path ?l1 ?l2)
        (has ?c ?f)
        (alive ?c)
        (safe ?l)
        (fed ?c)
        (cured ?c)
        (infected ?c)
        (isbaby ?c)
        (ishero ?c)
        (israwfood ?f)
        (iskitchen ?l)
        (isfoodstore ?l)
    )

    (:action buildkitchen
        :parameters (
            ?c - character
            ?l - location
        )
        :precondition (and 
            (alive ?c)
            (at ?c ?l)
            (safe ?l)
        )
        :effect (and 
            (iskitchen ?l)
        )
    )
    (:action growfood
        :parameters (
            ?f - food
            ?l - location
        )
        :precondition (and 
            (safe ?l)
        )
        :effect (and 
            (israwfood ?f)
            (at ?f ?l)
        )
    )
    (:action buildfoodstore
        :parameters (
            ?c - character
            ?l - location
        )
        :precondition (and 
            (alive ?c)
            (at ?c ?l)
            (safe ?l)
        )
        :effect (and 
            (isfoodstore ?l)
        )
    )
    
    
    
    (:action magicalMove
        :parameters (
            ?c - character
            ?l - location
        )
        :precondition (and 
            ; (not (alive ?c))
            (safe ?l)
        )
        :effect (and 
            ; (alive ?c)
            (at ?c ?l)
        )
    )
    
    (:action gocharacter
        :parameters (
            ?c - character 
            ?l1 - location 
            ?l2 - location
        )
        :precondition (and 
            (alive ?c)
            (path ?l1 ?l2) 
            (at ?c ?l1) 
            )
        :effect (and 
            (at ?c ?l2) 
            (not (at ?c ?l1))
        )
    )

    (:action gozombie
        :parameters (
            ?z - zombie 
            ?l1 - location 
            ?l2 - location
        )
        :precondition (and 
            (path ?l1 ?l2) 
            (at ?z ?l1)
        )
        :effect (and 
            (at ?z ?l2) 
            (not (at ?z ?l1)) 
            (safe ?l1) 
            (not(safe ?l2))
        )
    )

    (:action diecharacter
        :parameters (
            ?c - character 
            ?l - location 
        )
        :precondition (and 
            (alive ?c)
            (isbaby ?c)
            (at ?c ?l) 
        )
        :effect (and             
            (not(alive ?c))
        )
    )

    (:action getattacked
        :parameters (
            ?c - character 
            ?z - zombie
            ?l - location 
        )
        :precondition (and 
            (alive ?c)
            (at ?c ?l) 
            (at ?z ?l)
        )
        :effect (and 
            (infected ?c)
        )
    )

    (:action getcured
        :parameters (
            ?c - character
            ?a - antidote
            ?l - location
        )
        
        :precondition (and 
            (at ?c ?l)
            (has ?c ?a)
            (safe ?l)
            (infected ?c)
        )
        :effect (and 
            (cured ?c)
            (not (fed ?c))
        )
    )
    
(:action giveantidote
    :parameters (
            ?c1 - character
            ?c2 -  character
            ?a - antidote
            ?l - location
    )
    :precondition (and 
        (alive ?c1)
        (alive ?c2)
        (at ?c1 ?l)
        (at ?c2 ?l)
        (safe ?l)
        (has ?c1 ?a)
    )
    :effect (and 
        (not(has ?c1 ?a))
        (has ?c2 ?a)
    )
)

    
(:action collectantidote
    :parameters (
        ?c - character
        ?a - antidote
        ?l - location
    )
    :precondition (and 
        (alive ?c)
        (ishero ?c)
        (at ?c ?l)
        (at ?a ?l)
    )
    :effect (and 
        (has ?c ?a)
        (not (at ?a ?l))
    )
)


    (:action collectrawfood
        :parameters (
            ?c - character 
            ?f - food
            ?l - location 
        )
        :precondition (and 
            (alive ?c)
            (israwfood ?f)
            (isfoodstore ?l)
            (at ?c ?l) 
            (at ?f ?l)
        )
        :effect (and 
            (has ?c ?f)
            (not(at ?f ?l))
        )
    )
    
    (:action cookfood
        :parameters (
            ?c - character 
            ?l - location 
            ?f - food
        )
        :precondition (and 
            (alive ?c)
            (isKitchen ?l)
            (israwfood ?f)
            (at ?c ?l)
            (has ?c ?f)
        )
        :effect (and 
            (not (israwfood ?f))
        )
    )

    (:action givefood
    :parameters (
            ?c1 - character
            ?c2 -  character
            ?f - food
            ?l - location
    )
    :precondition (and 
        (alive ?c1)
        (alive ?c2)
        (at ?c1 ?l)
        (at ?c2 ?l)
        (safe ?l)
        (has ?c1 ?f)
    )
    :effect (and 
        (not(has ?c1 ?f))
        (has ?c2 ?f)
    )
)
    (:action eatfood
        :parameters (
            ?c - character 
            ?l - location
            ?f - food
        )
        :precondition (and 
            (alive ?c)
            (safe ?l)
            (at ?c ?l)
            (has ?c ?f)
            (not(fed ?c))
            (not(israwfood ?f))
        )
        :effect (and 
            (fed ?c)
            (not (has ?c ?f))
        )
    )
    
    
    
)