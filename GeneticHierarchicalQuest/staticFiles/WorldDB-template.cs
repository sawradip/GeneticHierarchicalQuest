using System;
using System.Collections.Generic;
using System.Data.Common;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GeneticQuestTemplate
{
    class WorldDB
    {
        public string domainName;
        public Dictionary<string, char> WorldEvents = new();
        public Dictionary<string, List<string>> WorldObjects = new();
        public Dictionary<string, Predicate> WorldPredicates = new();
        public Dictionary<string, Action> WorldActions = new();
        public List<ActionStatement> WorldStaticInits = new();

        public string rootDir = @"C:\Users\sawra\Desktop\practice_code\practice_csharp\genetic_quest_sharp\genetic_quest_sharp";

        public WorldDB()
        {
            domainName = "GetDone";
            LoadWorldEvents();
            LoadWorldObjects();
            LoadWorldPredicates();
            LoadWorldStaticInits();
            LoadWorlActions();
            Console.WriteLine("Loaded all data");
        }

        public void LoadWorldObjects()
        {
            //WorldObjects["location"] = new List<string> { "outdoor1", "outdoor2", "walkway1",
            //                                            "walkway2", "walkway3", "walkway4",
            //                                            "walkway5", "walkway6", "sideway1",
            //                                            "sideway2",  "sideway3", "sideway4",
            //                                            "bedroom1", "bedroom2", "kitchen",
            //                                            "foodstore" };
        }
        public void LoadWorlActions()
        {
            //WorldActions["gocharacter"] = new Action('=');
            //WorldActions["gocharacter"].parameters =
            //    new Dictionary<string, string>() {  { "?c", "character" },
            //                                        { "?l1", "location"},
            //                                        { "?l2", "location"} };
            //WorldActions["gocharacter"].precondition =
            //    new List<ActionStatement> { new ActionStatement("(alive ?c)"),
            //                                new ActionStatement("(path ?l1 ?l2)"),
            //                                new ActionStatement("(at ?c ?l1)")};
            //WorldActions["gocharacter"].effect =
            //    new List<ActionStatement> { new ActionStatement("(at ?c ?l2)"),
            //                                new ActionStatement("(path ?l1 ?l2)"),
            //                                new ActionStatement("(at ?c ?l1)")};


        }

        public void LoadWorldPredicates()
        {
            PredicateParam pp = new PredicateParam();

            //WorldPredicates.Add("at", new Predicate(true, true, 2));
            //WorldPredicates["at"].paramList.Add(new PredicateParam(true, "character", "food", "antidote", "zombie"));
            //WorldPredicates["at"].paramList.Add(new PredicateParam("location"));

        }

        public void LoadWorldStaticInits()
        {
            //WorldStaticInits.Add(new ActionStatement("(at husband bedroom1)"));
        }
        public void LoadWorldEvents()
            {
                //WorldEvents["gocharacter"] = '=';
                foreach (var item in WorldActions)
                {
                    WorldEvents[item.Key] = item.Value.tensionLevel;
                }
            }

    }

    

    public class ActionStatement
    {
        string actionStringRaw = "";
        public bool ifWithNot = false;
        public List<string> actionList = new List<string>();

        public ActionStatement() { }
        public ActionStatement(string actionString)
        {
            actionStringRaw = actionString;
            actionString = actionString.Substring(1, actionString.Length - 2).Trim();

            foreach (string s in actionString.Split(" ").ToList())
            {
                actionList.Add(s);
            }
        }
        public ActionStatement(string actionString, bool _ifWithNot)
        {
            ifWithNot = _ifWithNot;
            actionStringRaw = actionString;
            actionString = actionString.Trim();
            if (ifWithNot)
            {
                int startIndex = actionString.IndexOf("(", 3);
                int endIndex = actionString.IndexOf(")");
                actionString = actionString.Substring(startIndex + 1, endIndex - startIndex - 2);

            }
            else
            {
                actionString = actionString.Substring(1, actionString.Length - 2).Trim();
            }

            foreach (string s in actionString.Split(" ").ToList())
            {
                actionList.Add(s);
            }
        }
        public ActionStatement(string action, params string[] paramList)
        {
            actionList.Add(action);
            foreach (string param in paramList)
            {
                actionList.Add(param);
            }
        }
        public string toString()
        {
            if (actionStringRaw != "")
            {
                return actionStringRaw;
            }

            string actionstring = "( ";

            foreach (string p in actionList)
            {
                actionstring += (p + " ");
            }

            actionstring += ")";
            return actionstring;
        }
    }

    public class Predicate
    {
        public bool initialstate;
        public bool goalstate;
        public int numParams;
        public string opposite = "";
        public List<PredicateParam> paramList = new();

        public Predicate(bool _initialstate, bool _goalstate, int _numparams)
        {
            this.initialstate = _initialstate;
            this.goalstate = _goalstate;
            this.numParams = _numparams;
        }

    }

    public class Action
    {
        public Dictionary<string, string> parameters = new();
        public List<ActionStatement> precondition = new();
        public List<ActionStatement> effect = new();
        public char tensionLevel;

        public Action(char c)
        {
            this.tensionLevel = c;
        }

    }

    public class PredicateParam
    {
        public bool isUnique = false;
        List<string> paramAlternates = new List<string>();

        public PredicateParam() { }
        public PredicateParam(params string[] paramList)
        {
            foreach (string param in paramList)
            {
                paramAlternates.Add(param);
            }
        }
        public PredicateParam(bool _isUnique, params string[] paramList)
        {
            this.isUnique = _isUnique;
            foreach (string param in paramList)
            {
                paramAlternates.Add(param);
            }
        }


        public string getParam()
        {
            string myParam = paramAlternates[(new Random()).Next(0, paramAlternates.Count)];
            return myParam;
        }
    }

}
